# Introduction
moco_dashboard_core is a base framework for all __Aksaramaya's Dashboard__ products from various Services, we use React as based because React allows developers to create large web applications which can change data, without reloading the page. The main purpose of React is to be fast, scalable, and simple.

We will have another repository for sample implementation of __moco_dashboard_core__, in that implementation we will showcase various case which have been implemented before, at it will always be improved

moco_dashboard_core is based on the following frameworks:
* React-Admin 3.0 https://marmelab.com/react-admin/
* Material UI 3.0 https://material-ui.com/ 
* and many more ...

# Getting Started #
To get started you just need to
* clone this repository
* cd moco_dashboard_core
* copy .env.example to .env
* yarn start

you can view it on localhost:3000

# Difference with original React-Admin #
The following section will explain regarding the difference with original React-Admin framework and why
* __Custom Layout__ this is due to ReactAdmin does not have custom layout configurability and the possibility of layout is various on web application, the custom layout are mostly on __layouts__ folder and implemented on __App.js__
* __Multiple Data Provider__ yet this is another possibility that is not covered by ReactAdmin, Since ReactAdmin supports mostly the __jsonAPI__ API, other legacy provider are hard to be implemented through react admin standards. all the custom data provider should be put on __providers__ folder and registered on __DataProvider.js__.

# Features List #
- [x] Custom Layout
- [x] Multiple Data Provider
- [x] Integration with MBAAS
- [x] Theming
- [ ] Internationalization
- [ ] Google Firebase Analytics Integration
