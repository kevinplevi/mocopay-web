import gql from "graphql-tag";
export const getUser = gql`
  query getUser(
    $limit: Int
    $offset: Int
    $sort: UsersSortEnum
    $order: PaginationOrderEnum
  ) {
    allUsersList(limit: $limit, offset: $offset, sort: $sort, order: $order) {
      data {
        id
        created_at
        updated_at
        roles
        email
        password
        social_ids
        verified
        fcm_tokens
        name
        deactivated_at
        organization_id
      }
      count
    }
  }
`;
