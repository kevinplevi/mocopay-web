import store from "../store";
import * as ActionType from "../action-type";

export default {
  addCount: (value) => {
    const action = { type: ActionType.ADD_COUNT };
    store.dispatch(action);
  },
  getCount: (value) => {
    const action = { type: ActionType.GET_COUNT };
    store.dispatch(action);
  },
};
