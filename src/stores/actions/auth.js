import store from "../store";
import * as ActionType from "../action-type";
import client from "../../utils/mocosdk";

export default {
  setAuth: async (value) => {
    const login = await client.auth.login("local", {
      email: value.username,
      password: value.password,
    });
    console.log(login);
    if (login.error) {
      console.log("error login");
      return false;
    } else {
      localStorage.setItem("token", JSON.stringify(login));
      const action = { type: ActionType.AUTH_SET, value };
      await store.dispatch(action);
      return true;
    }
  },
  clearAuth: (value) => {
    const action = { type: ActionType.AUTH_CLEAR };
    store.dispatch(action);
  },
};
