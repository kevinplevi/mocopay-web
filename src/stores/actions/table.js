import store from "../store";
import * as ActionType from "../action-type";
import client from "../../utils/mocosdk";

const getTableGQL = async (value) => {
  const table = client.gql.query(value);
  return table;
};

export default {
  getTable: async (value) => {
    const table = client.getTable(value);
    const tableRead = await table.read();
    const action = {
      type: ActionType.GET_TABLE,
      value: { data: tableRead.data, name: value.replace("_", "") },
    };
    store.dispatch(action);
    return tableRead;
  },
  getUsersTable: async (value) => {
    const getData = await getTableGQL(value);
    const action = {
      type: ActionType.GET_TABLE,
      value: { data: getData.allUsersList.data, name: "users" },
    };
    store.dispatch(action);
    return getData;
  },
};
