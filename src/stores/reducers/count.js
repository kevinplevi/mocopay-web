import * as ActionType from "../action-type";

const initState = {
  count: 100,
};

const reducer = (state = initState, action) => {
  const { type, value } = action;

  switch (type) {
    case ActionType.ADD_COUNT:
      //   console.log({ ...state, count: state.count + 1 });
      return { ...state, count: state.count + 1 };
    case ActionType.GET_COUNT:
      return state.count;
    default:
      return state;
  }
};

export default reducer;
