import auth from "./auth";
import lang from "./lang";
import count from "./count";
import table from "./table";

export default { auth, lang, count, table };
