import * as ActionType from "../action-type";

const initializeState = () => {
  const table = {
    users: [],
    payments: [],
    paymenthistories: [],
    paymentfees: [],
    organizations: [],
    downloads: [],
    apicredentials: [],
  };
  return table;
};

const reducer = (state = initializeState(), action) => {
  const { type, value } = action;
  switch (type) {
    case ActionType.GET_TABLE:
      return { ...state, [value.name]: value.data };
    default:
      return state;
  }
};

export default reducer;
