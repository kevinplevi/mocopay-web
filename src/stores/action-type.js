export const AUTH_SET = "AUTH_SET";
export const AUTH_CLEAR = "AUTH_CLEAR";

export const LANG_SET = "LANG_SET";

export const ADD_COUNT = "ADD_COUNT";
export const GET_COUNT = "GET_COUNT";

export const GET_TABLE = "GET_TABLE";
