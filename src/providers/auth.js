import authAction from "../stores/actions/auth";

export default {
  login: async (params) => {
    const login = await authAction.setAuth(params);
    return login;
  },
  logout: () => {
    authAction.clearAuth();
    return Promise.resolve();
  },
};
