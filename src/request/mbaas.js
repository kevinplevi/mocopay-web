import axios from "axios";

const request = (method, url, params, data) => {
  const headers = {
    "Content-Type": "application/vnd.api+json",
    Accept: "application/vnd.api+json",
  };

  const token = localStorage.getItem("token");
  if (token) headers.Authorization = `Bearer ${token}`;

  return axios({
    method,
    url,
    headers,
    params: params,
    data: data,
  });
};

export default request;
