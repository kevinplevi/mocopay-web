import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

import oAuth from "../../providers/auth";

class Login extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    window.document.title = "Login";
    const getUser = localStorage.getItem("token");
    if (getUser) {
      console.log(this.props.history);
      this.props.history.push("/");
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const login = await oAuth.login(this.state);

    if (login) {
      console.log("login sukses");
      this.setState({ error: false });
      this.props.history.push("/");
    } else {
      this.setState({ error: true });

      console.log("login error page");
    }
  };

  handleChange(e) {
    this.setState({ ...this.state, [e.target.name]: e.target.value });
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col lg="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup
                        className="mb-3"
                        style={{
                          border: this.state.error
                            ? "1px solid red"
                            : "0px solid grey",
                        }}
                      >
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Username"
                          autoComplete="username"
                          onChange={this.handleChange}
                          name="username"
                          value={this.state.username}
                          required
                        />
                      </InputGroup>
                      <InputGroup
                        className="mb-4"
                        style={{
                          border: this.state.error
                            ? "1px solid red"
                            : "0px solid grey",
                        }}
                      >
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          onChange={this.handleChange}
                          name="password"
                          value={this.state.password}
                          required
                        />
                      </InputGroup>
                      <Row>
                        {this.state.error && (
                          <div
                            style={{
                              color: "red",
                              marginLeft: "20px",
                              marginBottom: "10px",
                            }}
                          >
                            login failed, invalid user
                          </div>
                        )}
                      </Row>
                      <Row>
                        <Col xs="6">
                          <Button
                            color="primary"
                            onSubmit={this.handleSubmit}
                            className="px-4"
                          >
                            Login
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">
                            Forgot password?
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
