/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import Content from "../../../components/Content";
import { Card, CardHeader, CardContent, Button } from "@material-ui/core";
import tableActions from "../../../stores/actions/table";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import i18next from "i18next";
import { getUser } from "../../../graphql/Users";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  refresh: {
    marginBottom: "20px",
  },
});

const App = (props) => {
  const [model] = useState({
    title: "User Table",
    breadcrumbs: [{ text: "Home", to: "/" }, { text: "Users" }],
  });
  const { tableData } = props;
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [limit, setLimit] = useState(10);
  const [offset, setOffset] = useState(0);
  const [sort, setSort] = useState("created_at");
  const [order, setOrder] = useState("ASC");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setLoading(true);
    const dataGQL = await tableActions.getUsersTable(
      {
        query: getUser,
        variables: {
          limit,
          offset,
          sort,
          order,
        },
      },
      "users"
    );
    console.log(dataGQL);
    setLoading(false);
  };

  return (
    <Content breadcrumbs={model.breadcrumbs}>
      <Card>
        <CardHeader title={i18next.t("table.userTable")} />
        <CardContent>
          <Button
            onClick={fetchData}
            color="primary"
            variant="contained"
            className={classes.refresh}
          >
            {!loading ? "Refresh" : "Loading..."}
          </Button>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>ID</TableCell>
                  <TableCell>Password</TableCell>
                  <TableCell>Created_at</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell>{row.email}</TableCell>
                    <TableCell>{row.id}</TableCell>
                    <TableCell>{row.password}</TableCell>
                    <TableCell>{row.created_at}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>
    </Content>
  );
};

const mapStateToProps = (state) => {
  return {
    tableData: state.table.users,
    count: state.count,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCountAction: () => {
      dispatch({ type: "ADD_COUNT" });
    },
  };
};

const page2 = connect(mapStateToProps, mapDispatchToProps)(App);
export default page2;
