import React, { useState, useEffect } from "react";
import Content from "../../../components/Content";
import { Card, CardHeader, CardContent, Button } from "@material-ui/core";
import tableActions from "../../../stores/actions/table";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import i18next from "i18next";
import TablePagination from "@material-ui/core/TablePagination";

const columns = [
  { id: "id", label: "ID", minWidth: 100 },
  { id: "created_at", label: "created_at", minWidth: 100 },
  {
    id: "updated_at",
    label: "updated_at",
    minWidth: 170,
  },
  {
    id: "name",
    label: "name",
    minWidth: 170,
  },
  {
    id: "address",
    label: "address",
    minWidth: 170,
  },
  {
    id: "phone",
    label: "phone",
    minWidth: 170,
  },
  {
    id: "documents",
    label: "documents",
    minWidth: 170,
  },
];

function createData(
  id,
  created_at,
  updated_at,
  name,
  address,
  phone,
  documents
) {
  return { id, created_at, updated_at, name, address, phone, documents };
}

const rows = [
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org1",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
  createData(
    "org2",
    Date.now(),
    Date.now(),
    "Organization-1",
    "Earth",
    "098989990",
    "doc-1"
  ),
];

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  refresh: {
    marginBottom: "20px",
  },
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

const App = (props) => {
  const [model] = useState({
    title: i18next.t("table.orgTable"),
    breadcrumbs: [
      { text: "Home", to: "/" },
      { text: i18next.t("table.orgTable") },
    ],
  });
  const { tableData } = props;
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  useEffect(() => {
    fetchData();
    document.title = i18next.t("table.orgTable");
  }, []);

  const fetchData = async () => {
    setLoading(true);
    const data = await tableActions.getTable("organizations");
    console.log(data);
    setLoading(false);
    // setcount(stores.getState("count").count);
  };

  return (
    <Content breadcrumbs={model.breadcrumbs}>
      <Card>
        <CardHeader title={i18next.t("table.orgTable")} />
        <CardContent>
          <Button
            onClick={fetchData}
            color="primary"
            variant="contained"
            className={classes.refresh}
          >
            {!loading ? "Refresh" : "Loading..."}
          </Button>
          {/* <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>id</TableCell>
                  <TableCell>created_at</TableCell>
                  <TableCell>updated_at</TableCell>
                  <TableCell>name</TableCell>
                  <TableCell>address</TableCell>
                  <TableCell>phone</TableCell>
                  <TableCell>documents</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData.map((row) => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell>{row.created_at}</TableCell>
                    <TableCell>{row.updated_at}</TableCell>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.address}</TableCell>
                    <TableCell>{row.phone}</TableCell>
                    <TableCell>{row.documents}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer> */}
          <Paper className={classes.root}>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
        </CardContent>
      </Card>
    </Content>
  );
};

const mapStateToProps = (state) => {
  return {
    tableData: state.table.payments,
    count: state.count,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCountAction: () => {
      dispatch({ type: "ADD_COUNT" });
    },
  };
};

const payments = connect(mapStateToProps, mapDispatchToProps)(App);
export default payments;
