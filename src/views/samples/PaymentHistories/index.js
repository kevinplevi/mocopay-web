import React, { useState, useEffect } from "react";
import Content from "../../../components/Content";
import { Card, CardHeader, CardContent, Button } from "@material-ui/core";
import tableActions from "../../../stores/actions/table";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import i18next from "i18next";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  refresh: {
    marginBottom: "20px",
  },
});

const App = (props) => {
  const [model] = useState({
    title: "Payments Histories",
    breadcrumbs: [
      { text: "Home", to: "/" },
      { text: i18next.t("table.paymentsTable"), to: "payments" },
      { text: i18next.t("table.paymentHistoriesTable") },
    ],
  });
  const { tableData } = props;
  const classes = useStyles();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setLoading(true);
    const data = await tableActions.getTable("payment_histories");
    console.log(data);
    setLoading(false);
  };

  return (
    <Content breadcrumbs={model.breadcrumbs}>
      <Card>
        <CardHeader title={i18next.t("table.paymentHistoriesTable")} />
        <CardContent>
          <Button
            onClick={fetchData}
            color="primary"
            variant="contained"
            className={classes.refresh}
          >
            {!loading ? "Refresh" : "Loading..."}
          </Button>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>id</TableCell>
                  <TableCell>created_at</TableCell>
                  <TableCell>updated_at</TableCell>
                  <TableCell>payment_id</TableCell>
                  <TableCell>status</TableCell>
                  <TableCell>remark</TableCell>
                  <TableCell>metadata</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData.map((row) => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell>{row.created_at}</TableCell>
                    <TableCell>{row.updated_at}</TableCell>
                    <TableCell>{row.password}</TableCell>
                    <TableCell>{row.payment_id}</TableCell>
                    <TableCell>{row.status}</TableCell>
                    <TableCell>{row.remark}</TableCell>
                    <TableCell>{row.metadata}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>
    </Content>
  );
};

const mapStateToProps = (state) => {
  return {
    tableData: state.table.paymenthistories,
    count: state.count,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCountAction: () => {
      dispatch({ type: "ADD_COUNT" });
    },
  };
};

const paymentHistories = connect(mapStateToProps, mapDispatchToProps)(App);
export default paymentHistories;
