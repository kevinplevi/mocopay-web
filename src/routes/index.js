import React from "react";

const routes = [
  { path: "/home", component: React.lazy(() => import("../views/Home")) },
  {
    path: "/sample",
    exact: true,
    component: React.lazy(() => import("../views/samples")),
  },
  {
    path: "/users",
    component: React.lazy(() => import("../views/samples/Users")),
  },
  {
    path: "/payments",
    component: React.lazy(() => import("../views/samples/Payments")),
  },
  {
    path: "/payment-histories",
    component: React.lazy(() => import("../views/samples/PaymentHistories")),
  },
  {
    path: "/payment-fees",
    component: React.lazy(() => import("../views/samples/PaymentFees")),
  },
  {
    path: "/organizations",
    component: React.lazy(() => import("../views/samples/Organizations")),
  },
  {
    path: "/downloads",
    component: React.lazy(() => import("../views/samples/DownloadsPage")),
  },
  {
    path: "/api-cred",
    component: React.lazy(() => import("../views/samples/ApiCred")),
  },
];

export default routes;
