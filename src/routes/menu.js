const menu = {
  items: [
    { name: "Home", url: "/home", icon: "icon-home" },
    { title: true, name: "Members" },
    { name: "Users", url: "/users", icon: "icon-user" },
    { name: "Organizations", url: "/organizations", icon: "icon-people" },
    { title: true, name: "Transaction" },
    { name: "Payments", url: "/payments", icon: "icon-basket" },
    { name: "Payment Fees", url: "/payment-fees", icon: "icon-pie-chart" },
    { title: true, name: "Settings" },
    { name: "Downloads", url: "/downloads", icon: "icon-cloud-download" },
    { name: "Api Credentials", url: "/api-cred", icon: "icon-wrench" },
  ],
};

export default menu;
